---
title: "About"
date: 2023-09-06T09:00:18+08:00
draft: false
categories: [adm]
---

![](https://www.computerhope.com/jargon/i/initialization.jpg)

### 初始設定

這是統計軟體與實務應用2022課程網頁。       Curator：Kno Tsao

資料的取得、面向與規模隨著網路發展超速成長。統計是資料科學中的一個重要主軸。本課程將透過引介統計軟體 R, Rstudio, 以及網站 R bloggers, Github, Kaggle 引發學生在資料科學分析與實作的第一步。教學計畫表 syllabus 部份的資料與內容可能將視課程進行狀況略做調整。

以上是官方的說法。真實的設定是  ——— **R games**. 

有興趣同學也可參考一下[2022版課網](https://chtsao.gitlab.io/rgame22/)。


### 課程資訊

* [Syllabus](https://chtsao.gitlab.io/rgame23/rgame23.sylla.pdf). Curator: C. Andy Tsao (Kno).  Office: SE A411.  Tel: 3520
* Lectures: 
  * 3D: Mon. 1510-1600, Wed. 1310-1500 @ SE A211.
* Office Hours:  Mon. 14:10-15:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎，但若有導生來，則導生優先)
* TA Office Hours: 
	* 張秝穎 (二) 1500～1600 @SE A412  611111102@gms.ndhu.edu.tw	
	* 李錦州 (三) 1300～1400 @SE A408  611211104@gms.ndhu.edu.tw
* [數學小天地 (NEW) @理 A307 教室](http://faculty.ndhu.edu.tw/~yltseng/help.html)
* Prerequisites: Intro to Probability, Statistics (taken or taking) and **curiosity about data/world/parallel worlds**.
