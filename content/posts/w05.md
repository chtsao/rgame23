---
title: "Week 5. Projects -- A Closer Look"
date: 2023-10-10T14:44:30+08:00
draft: false
categories: [team, game]
tags: [project, books]
---
<img src="https://i1.wp.com/www.stugon.com/wp-content/uploads/2013/10/im_a_geek_wallpaper_by_thegraphicgeek.png" style="zoom:50%;" />

## 10/25 (Wed) 分組提案/構想報告
每組 10 分鐘。包含
1. Data download and import to R. 

2. Project Overview: Main Questions.

3. 初步 Exploratory Data Analysis. 
    優先使用 R notebook preview/html 報告。

  準備/報告預想幾個問題：

1. 我們想回答什麼問題？這個問題為什麼有趣/重要？與你（TA/閱聽者）有何關聯/影響？

2. 我們使用哪個資料來處理這個問題？關聯性與限制性。

3. 我們想用哪種角度/分析方法來處理這個問題？佐證：初步的EDA：這資料適合＋我們有這個能力/潛力執行。 

## Data Science Projects 

1. [Movie Recommendation](https://data-flair.training/blogs/data-science-r-movie-recommendation/) from [Data-Flair](https://data-flair.training/)
2. [Top 5 Data Science Projects with Source Code to kick-start your Career](https://data-flair.training/blogs/data-science-projects-code/)
3. [Kaggle Data Sets](https://www.kaggle.com/datasets), [Interesting Kaggle Datasets Every Beginner in Data Science Should Try Out](https://www.analyticsvidhya.com/blog/2021/04/interesting-kaggle-datasets-every-beginner-in-data-science-should-try-out/)

## Taxonomy of  Problems: ML/Stat

#### Formulation

#### Machine Learning Language

* Supervised Learning ("with $Y$")
  1. Regression: $Y \approx f(X_1, \cdots, X_p) $ where $Y$ is a *continuous*
  2. Classification:  $ Y \approx f(X_1, \cdots, X_p) $ where $Y$ is a *categorical*
* Unsupervised Learning ("without $Y$")
  1. Cluster Analysis
  2. Principal Component Analysis

#### Statistics Language

* "W/ $Y$"
  1. GLM (General Linear Model): Regression, ANOVA, ANCOVA: $Y$ is a *continuous*
  2. GLIM (Generalized Linear Model): 
     * Logistic Regression, Loglinear Model ($Y$ is a *categorical*) 
     * GLM. 
     * [GLIM Table](https://online.stat.psu.edu/stat504/node/216/): family and link
  3. FDA (Fisher Discriminant Analysis) and likelihood-ratio-based methods
* "W/O $Y$": PCA, MDS, Factor Analysis

### Links and Read

1. [YaRrr! The Pirate’s Guide to R](https://bookdown.org/ndphillips/YaRrr/). Install $\tt{yarrr}$ package to run the codes in the book. 
2. [R Tutorial](https://www.tutorialspoint.com/r/index.htm) 
3. [R for Data Science](https://r4ds.had.co.nz/) and other [books](https://rstudio.com/resources/books/) from [Rstudio](https://rstudio.com/)
4. [Fundamentals of Data Visualization](https://serialmentor.com/dataviz/)
5. [10 things R can do that might surprise you](https://simplystatistics.org/2019/03/13/10-things-r-can-do-that-might-surprise-you/) @ [SimplyStatistics](https://simplystatistics.org/)
