---
title: "Week 3. Data Summary: Numerical and Graphical"
date: 2023-09-25T04:45:57+08:00
draft: false
tags: [notes, lab]
---
<img src="https://i.ytimg.com/vi/Qf3Y71aUe4w/maxresdefault.jpg" style="zoom:50%;" />

### Overview

Data Sets (dataframed in R)

* Start: datasets, mlbench etc. See 
  * [R built-in datasets](http://www.sthda.com/english/wiki/r-built-in-data-sets) 
  * [R dataset](https://www.programiz.com/r/dataset)
  * [Machine Learning Datasets in R (10 datasets you can use right now)](https://machinelearningmastery.com/machine-learning-datasets-in-r/)

[Data Summary (for one variable)](https://chtsao.gitlab.io/rgame22/posts/w02/#data-summary) 

#### Two variables

* Scatter plot: Trend, Cycles, Errors/Variation
* Correlation and  Variation
* Numerical summary is not enough. 
  * Anscombe's Quartet: [Wiki](https://en.wikipedia.org/wiki/Anscombe%27s_quartet),[@TDS](https://towardsdatascience.com/importance-of-data-visualization-anscombes-quartet-way-a325148b9fd2), [Rpub](https://rpubs.com/debosruti007/anscombeQuartet) 
  * [Datasaurus Dozen](https://blog.revolutionanalytics.com/2017/05/the-datasaurus-dozen.html) @revolutionanalytics.com


#### Many variables

* Paired plots
* Correlation Matrix
* Numerical summary is not enough---**But**..

## Codes, Readings and Links

* R commands: `View(), head(), tail(); summary(), plot(), pair(); stem(), hist(), boxplot()`
* Rstudio Keywords/Jargons: `Rnotebook, Quarto, Rmarkdown, Rscript, Preview/Knitr; packages, datasets, dataframe`
* Lab 1: [da001.qmd](https://chtsao.gitlab.io/rgame23/da001.qmd), [da001,html](https://chtsao.gitlab.io/rgame23/da001.html)
