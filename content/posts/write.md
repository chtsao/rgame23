---
title: "Write a Data Science Project Report"
date: 2023-12-27T10:43:02+08:00
draft: false
categories: [notes]
tags: [write]
---

<img src="http://harrisonamy.com/wp-content/uploads/2010/09/buffet.jpg" style="zoom: 25%;" />

>寫報告不是從頭寫到尾，也很少一次完成。之前我們將閱讀技術內容，如 paper, 教科書，類比於吃Buffet。換個角度，撰寫這類的技術報告的過程，就是為可能的讀者/顧客呈現出一個好的 Buffet 的規劃，準備，安排與完成。
>
>Kn$\phi$

0. [A 麥開講：Ep 1. 數統其實(第一次學)大家都不懂 與 學習學習小米 和 Forever 學姐長分享](https://open.spotify.com/episode/3o8szfsW6a9ZCjyy9akp6Y?si=OKPHGI3aTpuVmY3ckN9IOQ&nd=1&dlsi=4db88b7fcf784916)
1. 可以先給個內部給親朋好有的熱身演講，整理一下你現在有的內容。[參考](https://chtsao.gitlab.io/rgame22/posts/sup00/)。
2. 有時 由上往下寫，有時 由下往上寫
    上往下 指架構->細部文句，由 Chapter -> Section -> Paragraph -> Subparagraph -> sentences. 下往上則反之。
3. 由 Data and Methods, Results 部份開始寫一般比較容易。
4. Results: Visualization 如主要的 Figures and Tables 要先確認
5. References： 加入適當的 annotation 對後續撰寫會有幫助。
6. Write and Rewrite; Organize and Reorganize。
7. Introduction, Conclusions 最後寫。準備好回答這些問題
   * 這個研究的主要發現/創新是什麼？之前相關研究的優/缺點？這個研究與它們的對比？
   * 總結這個研究：一句話，一張圖，一張表 ：(開始/Intro) and  (結束/Conclusions)
   * 你的貢獻？對我（讀者/聽者）有何幫助？

#### References and Links
* [Pinker's 13 Rules on good writing](https://www.openculture.com/2019/03/steven-pinkers-13-rules-for-good-writing.html)
* [How to Write a Data Science Project Report?](https://www.projectpro.io/article/data-science-project-report/620)
* Former [A408ers](https://etd.ndhu.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=zvBRP0/result): [劉映彤](https://chtsao.gitlab.io/rgame22/FragranceMap.pdf), [林君濠](), [施承翰](), [呂一昕](), [陳學蒲](https://etd.ndhu.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=zvBRP0/record?r1=2&h1=0#XXXX),  [何尚謙](https://etd.ndhu.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=zvBRP0/search?q=auc=%22%E4%BD%95%E5%B0%9A%E8%AC%99%22.&searchmode=basic), [劉雅涵](https://etd.ndhu.edu.tw/cgi-bin/gs32/gsweb.cgi/ccd=zvBRP0/search?q=auc=%22%E5%8A%89%E9%9B%85%E6%B6%B5%22.&searchmode=basic#XXXX) 等。

