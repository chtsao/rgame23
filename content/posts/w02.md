---
title: "Week 2.  看圖說故事"
date:  2023-09-18T10:48:20+08:00
draft: false
categories: [note]
tags: [numerical summary, data visualization, story, R, Rscript, Rnotebook]
---
![](https://www.thenerdmag.com/wp-content/uploads/2019/03/Apex-Legends-Wallpaper.jpg)
### 野蠻世界
* [Microsoft Powe BI](https://powerbi.microsoft.com), [Tableu](https://www.tableau.com/)
* [Top 16 Types of Chart in Data Visualization](https://towardsdatascience.com/top-16-types-of-chart-in-data-visualization-196a76b54b62)
* [Flavor network](https://foodgalaxy.jp/FlavorNetwork/), [Flavor Network (Ahn, et al)](https://www.nature.com/articles/srep00196), [Linked Jazz](https://linkedjazz.org/network/)
* [Music-Map](https://www.music-map.com/)
* [g0v.中央政府總預算視覺化](http://budget.g0v.tw/budget), [台電資訊圖表](https://www.taipower.com.tw/tc/Chart.aspx?mid=194)

### Student Projects
* [League of Legends Data Visualization](https://studentwork.prattsi.org/infovis/projects/league-of-legends-data-visualization/)
* [S8 英雄特性分析與BP參考](https://m.gamer.com.tw/forum/C.php?bsn=17532&snA=644376)
* [Apex Legends](https://www.ea.com/games/apex-legends)

### 我考得如何？幾分會過？
* 動機問題：如何理解（老師/助教）公佈的成績資料？如何利用部份訊息來大致推估整體狀況？ 參考資料：[2016 統計學課網](http://faculty.ndhu.edu.tw/%7Echtsao/edu/16/stat/stat.html)
* 考得如何:  [Midterm Summary Stat 2021](https://chtsao.gitlab.io/stat21/posts/w09/) 
* 幾分會過: [I2P 2019 mid&final](http://faculty.ndhu.edu.tw/~chtsao/ftp/i2p/i2p2019w.nb.html)
* 關於(統計)預測這件事：抽樣，抽樣/推論母體，假設，模型預測準確性與其保證，整體/個別

### Data Summary

#### 重點

<u>Question</u>, <u>Obtain</u> (Sampling/Experimental Design), <u>Clean</u> and <u>Explore</u>

* 關於這個資料，關心的是什麼？ What’s my focal concern? Example: Music compression/MP3

* 登錄錯誤 不合邏輯 typo illogical errors
* 中央/中心 Center/Central Tendency

* 分散/變異 Spread/Variation

* 離群值 Outliers

* 常態與否 Normality (symmetry, modes, etc)

#### Numerical summary

- Mean, Median, Mode(s)

- Five-point summary

- Standard deviation, IQR (Q3-Q1)

#### Graphical summary

- stem-and-leaf plot
- Box plot
- Histogram
- Normal Probability Plot

#### Code, 閱讀及延伸

* R code: [datasummary.r](https://github.com/chtsao/rgames2017/blob/master/datasummary.r). `dataframe, plot(), summary()`
* Rnotebook: [rg03a.Rmd](https://chtsao.gitlab.io/rgame21/rg03a.Rmd), [rg03a.nb.html](https://chtsao.gitlab.io/rgame21/rg03a.nb.html); [rg03b.Rmd](https://chtsao.gitlab.io/rgame21/rg03b.Rmd), [rg03b.nb.html](https://chtsao.gitlab.io/rgame21/rg03b.nb.html).
* Read: DKLM (Stat2022 textbook, see [Stat2022 About](https://chtsao.gitlab.io/stat22/about/)) Ch. 15, Ch. 16. 
* 高竹嵐 (2023): [沒想到國中會考作文題目居然出現「統計圖表」，如果我轉生成國中生會怎麼寫？](https://www.thenewslens.com/article/185907)