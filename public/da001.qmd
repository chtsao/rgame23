---
title: "Lab 01: Data Summary I"
author: "Kno"
format: html
editor: visual
---

![](https://i1.wp.com/animenewsandfacts.com/wp-content/uploads/2020/10/spy-x-family.jpg?fit=1200%2C800&ssl=1)

## R Data Summary

# Create a quarto doc

First test project on data summary.

-   Create quarto doc
-   Load a R data.frame from R {datasets}
-   View, head and tail the data
-   Data summary
    -   Numerical: 5-point summary (min, Q1, Q2=median, Q3, max); mean, sd
    -   Graphical: stem-and-leaf, histogram, boxplot
    -   Render its html output

## Cars Data

```{r}
cars
```

```{r}
View(cars)
tail(cars)
```

#### Numerical summary

```{r}
summary(cars)
```

```{r}
mean(cars$speed)
sd(cars$dist)
```

#### Graphical Summary

```{r}
plot(cars)
```

```{r}
stem(cars$speed)
hist(cars$speed)
hist(cars$speed, main="Speed")
boxplot(cars$speed)
```

### LaTeX Examples

-   $\int_0^{2\pi}sin(x)dx = ?$
-   $\sum_{k=1}^\infty \frac{1}{k} = \infty$
-   $a x^2 + bx + c=0$

### Reference

1.  [Am web](http://am.ndhu.edu.tw)
2.  [Kno's Teaching Web](http://faculty.ndhu.edu.tw/~chtsao/edu/edu.html)
3.  Blah, Blah, Blah

## Your Turn

1.  Redo the steps but using **trees** data
2.  Do 1. in Rnotebook and knitr/render a html output
3.  Do 1. in Rscript
4.  Locate your output files in your notebook or desktop
